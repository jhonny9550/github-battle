import React from 'react';
import { string, func } from "prop-types";

function PlayerPreview(props) {
	return (
		<div>
			<div className='column'>
				<img
					className='avatar'
					src={props.avatar}
					alt={'Avatar for ' + props.username}
				/>
				<h2 className='username'>@{props.username}</h2>
			</div>
			{props.children}
		</div>
	)
}

PlayerPreview.propTypes = {
	avatar: string.isRequired,
	username: string.isRequired
}

module.exports = PlayerPreview;